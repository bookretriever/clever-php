<?php

namespace Clever;

use ArrayIterator;
use Clever\Exceptions\UnknownPropertyException;
use Clever\Internal\Obj;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use stdClass;

class CleverObject implements Countable, IteratorAggregate
{
    protected $data = [];

    public function __construct($data = [])
    {
        $this->load($data);
    }

    public function __clone()
    {
        // Empty so child classes can safely call parent::__clone()
    }

    public function __get($key)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        return null;
    }

    public function __isset($key)
    {
        return isset($this->data[$key]);
    }

    public function count()
    {
        return count($this->getArrayToIterate());
    }

    /**
     * Returns the value assigned to $key.
     *
     * @param int|string $key The key with which to fetch the value.
     *
     * @return mixed The value set for the $key.
     *
     * @throws UnknownPropertyException if $key is not set or the value is null
     */
    public function get($key)
    {
        if (!is_string($key) && !is_int($key)) {
            throw new InvalidArgumentException('$key must be a string or integer');
        }

        if (isset($this->data[$key])) {
            return $this->data[$key];
        }

        throw new UnknownPropertyException($key);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->getArrayToIterate());
    }

    public function toArray()
    {
        return $this->data;
    }

    protected function getArrayToIterate()
    {
        return $this->toArray();
    }

    private function load($data)
    {
        if ($data instanceof self) {
            $data = $data->data;
        }

        if (!is_array($data) && !($data instanceof stdClass)) {
            throw new InvalidArgumentException('data must be array or stdClass. got ' . Obj::getType($data));
        }
        $output = [];
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = new self($value);
            } elseif ($value instanceof stdClass) {
                $subvalues = [];
                foreach ($value as $subkey => $subvalue) {
                    $subvalues[$subkey] = $subvalue;
                }

                $value = new self($subvalues);
            }

            $output[$key] = $value;
        }

        $this->data = $output;
    }
}
