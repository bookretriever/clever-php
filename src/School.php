<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Contracts\LastModified;
use Clever\Contracts\SisObject;
use Clever\Internal\DistrictResource;
use Clever\Internal\EventableTrait;
use Clever\Internal\LastModifiedTrait;
use Clever\Internal\SisObjectTrait;
use Clever\Internal\Util;

class School extends DistrictResource implements Eventable, LastModified, SisObject
{
    use EventableTrait;
    use SisObjectTrait;
    use LastModifiedTrait;

    public function getGradeLowNumeric()
    {
        return Util::gradeToNumeric($this->low_grade);
    }

    public function getGradeHighNumeric()
    {
        return Util::gradeToNumeric($this->high_grade);
    }

    public function getMdrNumber()
    {
        return $this->mdr_number;
    }

    public function getName()
    {
        return $this->get('name');
    }

    public function getNcesId()
    {
        return $this->nces_id;
    }

    public function getStateId()
    {
        return $this->state_id;
    }

    public function getSchoolNumber()
    {
        return $this->school_number;
    }

    public function getType()
    {
        return 'school';
    }

    public function isMatchingGrade($originalGrade)
    {
        $grade = Util::gradeToNumeric($originalGrade);

        $low = $this->getGradeLowNumeric();
        $high = $this->getGradeHighNumeric();

        if ($grade === null) {
            if ($low !== null || $high !== null) {
                throw new InvalidArgumentException('Cannot check for matching grade with indeterminate grade \''
                    . $originalGrade . '\'');
            }
        }

        if ($low !== null && $grade < $low) {
            return false;
        }

        if ($high !== null && $grade > $high) {
            return false;
        }

        return true;
    }
}
