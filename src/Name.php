<?php

namespace Clever;

use Clever\Internal\Str;

class Name extends CleverObject
{
    public function getFormal()
    {
        return $this->format('L, F M');
    }

    public function getFormalShort()
    {
        return $this->format('L, F');
    }

    public function getFull()
    {
        return $this->format('F M L');
    }

    public function getFullShort()
    {
        return $this->format('F L');
    }

    private function format($str)
    {
        $replacements = [
            'F' => $this->first ?: '',
            'M' => $this->middle ?: '',
            'L' => $this->last ?: '',
        ];

        $str = strtr($str, $replacements);
        $whitespace = str_split(Str::WHITESPACE_CHARS);
        $toTrim = array_merge($whitespace, [
            ',',
        ]);

        $str = Str::trim($str, $toTrim);
        $str = preg_replace('/\s+/', ' ', $str);

        return $str;
    }
}
