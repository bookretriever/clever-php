<?php

namespace Clever\Request;

use InvalidArgumentException;

class Options
{
    private $autoRetry = false;
    private $autoWait = false;
    private $bearerToken = null;

    public function getAutoRetry()
    {
        return $this->autoRetry;
    }

    public function getAutoWait()
    {
        return $this->autoWait;
    }

    public function getBearerToken()
    {
        return $this->bearerToken;
    }

    public function loadQueryParameters($query)
    {
        // No query parameters are set by this class, only child classes.
    }

    public function loadQueryString($query)
    {
        $parameters = [];
        parse_str($query, $parameters);

        $this->loadQueryParameters($parameters);
    }

    public function setAutoRetry($autoRetry)
    {
        if ($autoRetry === true) {
            $autoRetry = 3;
        } elseif ($autoRetry === false || $autoRetry === null) {
            $autoRetry = 0;
        } elseif (!is_int($autoRetry)) {
            throw new InvalidArgumentException('$autoRetry must be a non-negative int or bool');
        }

        $this->autoRetry = $autoRetry;

        return $this;
    }

    public function setAutoWait($autoWait)
    {
        $this->autoWait = (bool) $autoWait;

        return $this;
    }

    public function setBearerToken($bearerToken)
    {
        if ($bearerToken !== null) {
            if (!is_string($bearerToken) || !trim($bearerToken)) {
                throw new InvalidArgumentException('$bearerToken must be non-empty string or null');
            }
        }

        $this->bearerToken = $bearerToken;

        return $this;
    }

    public function toQueryParameters()
    {
        return [];
    }

    public static function fromQueryString($query)
    {
        $options = static::make();
        $options->loadQueryString($query);

        return $options;
    }

    public static function make()
    {
        return new static();
    }
}
