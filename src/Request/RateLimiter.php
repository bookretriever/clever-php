<?php

namespace Clever\Request;

class RateLimiter
{
    private $rateLimits = [];
    private static $instance = null;

    public function clean($all = false)
    {
        if ($all) {
            $this->rateLimits = [];

            return;
        }

        $now = time();
        foreach ($this->rateLimits as $token => $limit) {
            if ($limit->getNextReset() < $now) {
                unset($this->rateLimits[$token]);
            }
        }
    }

    public function getRateLimit($bearerToken)
    {
        if (!isset($this->rateLimits[$bearerToken])) {
            return null;
        }

        $rateLimit = $this->rateLimits[$bearerToken];
        if ($rateLimit->getNextReset() < time()) {
            unset($this->rateLimits[$bearerToken]);

            return null;
        }

        return $rateLimit;
    }

    public function update($bearerToken, RateLimit $rateLimit)
    {
        $this->rateLimits[$bearerToken] = $rateLimit;
    }

    public function wait($bearerToken)
    {
        $limit = $this->getRateLimit($bearerToken);
        if (!$limit) {
            return;
        }

        $remaining = $limit->getRemaining();
        $total = $limit->getLimit();

        $resetTime = $limit->getNextReset();
        $timeUntilReset = $resetTime - time();

        // If we have no quota remaining, wait for at least 1 second before continuing
        if ($remaining < 1) {
            if ($timeUntilReset < 0) {
                $this->sleep(1);
            } else {
                $this->sleep($timeUntilReset + 1);
            }

            return;
        }

        if ($timeUntilReset <= 0) {
            return;
        }

        // If our clock doesn't seem to match up with Clever's,
        // limit request to 20 per second
        if ($timeUntilReset > 60) {
            return $this->usleep(5 * 1e4);
        }

        // Scale timeUntilReset for better precision for usleep
        $timeUntilReset *= 1e6;

        $timeToWait = ($timeUntilReset * 1e6) / $remaining;
        $this->usleep($timeToWait);
    }

    public static function get()
    {
        if (!static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function sleep($seconds)
    {
        sleep($seconds);

        $this->clean();
    }

    private function usleep($microseconds)
    {
        usleep($microseconds);

        $this->clean();
    }
}
