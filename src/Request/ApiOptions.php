<?php

namespace Clever\Request;

class ApiOptions
{
    const BASE_URL_API = 'https://api.clever.com';
    const BASE_URL_OAUTH = 'https://clever.com';

    public $baseUrl;

    /**
     * @var string Case-insensitive HTTP method
     */
    public $method = 'get';

    public $headers = [];
    public $body = null;
    public $queryParameters = null;

    public $bearerToken;

    public $clientId;
    public $clientSecret;

    /**
     * @var int|float Timeout to receive response from server, in seconds
     */
    public $timeout = 10;

    /**
     * @var int|float Timeout to form TCP connection to server, in seconds
     */
    public $timeoutConnection = 5;

    private static $defaults = [];

    public function __construct()
    {
        $this->baseUrl = self::BASE_URL_API;

        foreach (static::$defaults as $key => $value) {
            $this->{$key} = $value;
        }
    }

    public static function setDefault($key, $value)
    {
        static::$defaults[$key] = $value;
    }
}
