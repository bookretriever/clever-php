<?php

namespace Clever\Request;

/**
 * Objects that can be used to make API requests against Clever.
 */
interface ApiRequestor
{
    public function send($url, ApiOptions $options = null);
}
