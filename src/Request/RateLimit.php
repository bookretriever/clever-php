<?php

namespace Clever\Request;

use Carbon\Carbon;
use JsonSerializable;
use Psr\Http\Message\MessageInterface as PsrResponse;

/**
 * Information about the Clever API rate limit.
 */
class RateLimit implements JsonSerializable
{
    protected $bucket;
    protected $limit;
    protected $now;
    protected $remaining;
    protected $reset;

    public function __construct($bucket, $limit, $remaining, $nextReset)
    {
        $this->setBucket($bucket);
        $this->setLimit($limit);
        $this->setRemaining($remaining);
        $this->setNextReset($nextReset);
        $this->now = Carbon::now()->timestamp;
    }

    /**
     * @return int The name of the rate limiting bucket.
     */
    public function getBucket()
    {
        return $this->bucket;
    }

    /**
     * @return int Total number of requests allowed in a time period
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @return int Unix timestamp for when the time period resets.
     */
    public function getNextReset()
    {
        return $this->reset;
    }

    /**
     * @return int The number of available requests remaining in this time period
     */
    public function getRemaining()
    {
        return $this->remaining;
    }

    /**
     * @return int The number of seconds until the time period resets
     */
    public function getSecondsUntilReset()
    {
        return $this->getNextReset() - $this->now;
    }

    public function jsonSerialize()
    {
        return [
            'bucket' => $this->getBucket(),
            'limit' => $this->getLimit(),
            'remaining' => $this->getRemaining(),
            'reset' => $this->getNextReset(),
            'secondsUntilReset' => $this->getSecondsUntilReset(),
        ];
    }

    private function setBucket($bucket)
    {
        $this->bucket = (string) $bucket;
    }

    private function setLimit($limit)
    {
        $limit = intval($limit);
        if ($limit <= 0) {
            throw new InvalidArgumentException('$limit must be an integer');
        }

        $this->limit = $limit;
    }

    private function setNextReset($nextReset)
    {
        $nextReset = intval($nextReset);
        if ($nextReset <= 0) {
            throw new InvalidArgumentException('$nextReset must be an integer');
        }

        $this->nextReset = $nextReset;
    }

    private function setRemaining($remaining)
    {
        $remaining = intval($remaining);
        if ($remaining <= 0) {
            throw new InvalidArgumentException('$remaining must be an integer');
        }

        $this->remaining = $remaining;
    }

    public static function fromPsrResponse(PsrResponse $response)
    {
        $get = function ($headerValues) {
            if (count($headerValues) > 0) {
                return $headerValues[0];
            }

            return null;
        };

        $keys = [
            'bucket',
            'limit',
            'remaining',
            'reset',
        ];

        $values = [];

        foreach ($keys as $key) {
            $value = $response->getHeader('x-ratelimit-' . $key);
            $value = $get($value);
            if ($value === null) {
                return static::unlimited();
            }

            $values[] = $value;
        }

        list($bucket, $limit, $remaining, $reset) = $values;

        return new self($bucket, $limit, $remaining, $reset);
    }

    public static function unlimited()
    {
        return new self('unlimited', 1000000, 1000000, Carbon::now()->timestamp - 5);
    }
}
