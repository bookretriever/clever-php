<?php

namespace Clever\Request;

use Clever\Internal\Arr;
use InvalidArgumentException;

class IndexOptions extends Options
{
    private $endingBefore = null;
    private $includeCount = null;
    private $limit = null;
    private $startingAfter = null;

    public function getEndingBefore()
    {
        return $this->endingBefore;
    }

    public function getIncludeCount()
    {
        return $this->includeCount;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function getStartingAfter()
    {
        return $this->startingAfter;
    }

    public function loadQueryParameters($query)
    {
        parent::loadQueryParameters($query);

        if (Arr::has($query, 'ending_before')) {
            $value = Arr::get($query, 'ending_before');
            $this->setEndingBefore($value);
        }

        if (Arr::has($query, 'count')) {
            $value = Arr::get($query, 'count');
            $this->setIncludeCount($value === 'true');
        }

        if (Arr::has($query, 'limit')) {
            $value = Arr::get($query, 'limit');
            if (is_numeric($value)) {
                $value = intval($value);

                $this->setLimit($value);
            }
        }

        if (Arr::has($query, 'starting_after')) {
            $value = Arr::get($query, 'starting_after');
            $this->setStartingAfter($value);
        }
    }

    public function setIncludeCount($includeCount)
    {
        if ($includeCount) {
            if ($this->getStartingAfter() !== null || $this->getEndingBefore() !== null) {
                throw new InvalidArgumentException('$includeCount cannot be used with the $startingAfter option');
            }

            if ($this->getEndingBefore() !== null) {
                throw new InvalidArgumentException('$includeCount cannot be used with the $endingBefore option');
            }

            $includeCount = true;
        } else {
            $includeCount = null;
        }

        $this->includeCount = $includeCount;
    }

    public function setEndingBefore($endingBefore)
    {
        $this->setRangeValue('endingBefore', $endingBefore);

        return $this;
    }

    public function setLimit($limit)
    {
        if ($limit !== null) {
            if (!is_int($limit) || $limit < 0) {
                throw new InvalidArgumentException('$limit must be non-negative int or null');
            }
        }

        $this->limit = $limit;

        return $this;
    }

    public function setStartingAfter($startingAfter)
    {
        $this->setRangeValue('startingAfter', $startingAfter);

        return $this;
    }

    public function toQueryParameters()
    {
        $params = parent::toQueryParameters();

        $value = $this->getEndingBefore();
        if ($value !== null) {
            $params['ending_before'] = $value;
        }

        $value = $this->getIncludeCount();
        if ($value !== null) {
            $params['count'] = $value ? 'true' : 'false';
        }

        $value = $this->getLimit();
        if ($value !== null) {
            $params['limit'] = $value;
        }

        $value = $this->getStartingAfter();
        if ($value !== null) {
            $params['starting_after'] = $value;
        }

        return $params;
    }

    private function setRangeValue($key, $value)
    {
        if ($value !== null) {
            if (!is_string($value) || !trim($value)) {
                throw new InvalidArgumentException('$' . $key . ' must be non-empty string or null');
            }

            if ($this->getIncludeCount()) {
                throw new InvalidArgumentException('$' . $key . ' cannot be used with the count option');
            }
        }

        $this->{$key} = $value;
    }
}
