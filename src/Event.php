<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Internal\Obj;
use Clever\Request\ApiRequestor;
use Clever\Request\IndexOptions;
use InvalidArgumentException;

class Event extends ApiResource
{
    const BUCKET_CREATED = 'created';
    const BUCKET_DELETED = 'deleted';
    const BUCKET_UPDATED = 'updated';

    public function __construct($details = [])
    {
        $details = $this->parseDetails($details);

        parent::__construct($details);
    }

    public function getBucket()
    {
        $type = $this->get('type');
        $parts = explode('.', $type, 2);

        return $parts[1];
    }

    public function getObject()
    {
        return $this->get('data')->get('object');
    }

    public function getType()
    {
        return 'event';
    }

    public static function getLatest(IndexOptions $options = null, ApiRequestor $requestor = null)
    {
        if (!$options) {
            $options = new IndexOptions();
        }

        $options->setLimit(1);
        $options->setEndingBefore('last');

        $resourceList = static::index($options, $requestor);
        if (!count($resourceList)) {
            return null;
        }

        return $resourceList->get(0);
    }

    public static function supportsCount()
    {
        return false;
    }

    protected function getEventTypeClass($eventType)
    {
        $resourceType = $this->getResourceType($eventType);

        $types = Types::all();
        foreach ($types as $class) {
            $emptyResource = new $class();
            if (!($emptyResource instanceof Eventable)) {
                continue;
            }

            if ($emptyResource->getEventType() === $resourceType) {
                return $class;
            }
        }

        return null;
    }

    protected function getResourceType($eventType)
    {
        if (!$eventType || !is_string($eventType)) {
            throw new InvalidArgumentException('$eventType');
        }

        $parts = explode('.', $eventType, 2);

        return $parts[0];
    }

    protected function parseDetails($details = [])
    {
        if (!$details) {
            return $details;
        }

        $eventType = Obj::get($details, 'type');

        if (!$eventType) {
            return $details;
        }

        $object = Obj::get($details, 'data.object');
        if (!$object) {
            return $details;
        }

        $newObject = $this->readObject($eventType, $object);

        Obj::set($details, 'data.object', $newObject);

        return $details;
    }

    protected function readObject($eventType, $objectData)
    {
        $resourceClass = $this->getEventTypeClass($eventType);
        if (!$resourceClass) {
            return $objectData;
        }

        return new $resourceClass($objectData);
    }
}
