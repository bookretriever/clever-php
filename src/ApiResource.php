<?php

namespace Clever;

use BadMethodCallException;
use Clever\Contracts\Linkable;
use Clever\Exceptions\ApiRequestException;
use Clever\Exceptions\UnexpectedResponseException;
use Clever\Internal\Arr;
use Clever\Internal\LinkableTrait;
use Clever\Internal\Obj;
use Clever\Internal\Uri;
use Clever\Request\ApiOptions;
use Clever\Request\ApiRequestor;
use Clever\Request\DefaultApiRequestor;
use Clever\Request\IndexOptions;
use Clever\Request\Options;
use Clever\Request\RateLimiter;
use Clever\Request\RetrieveOptions;

/**
 * A CleverObject that has an API endpoint.
 */
abstract class ApiResource extends CleverObject implements Linkable
{
    use LinkableTrait;

    public function getBasePath()
    {
        return implode('/', [
            'v1.1',
            $this->getIndexType(),
        ]);
    }

    public function getId()
    {
        return $this->get('id');
    }

    /**
     * Get the path for listing all resources.
     *
     * @return string[] The index path.
     */
    public function getIndexPath()
    {
        return [
            $this->getBasePath(),
        ];
    }

    public function getIndexType()
    {
        return $this->getType() . 's';
    }

    public function getLinkTypes()
    {
        return [
            'self' => $this->getType(),
        ];
    }

    /**
     * Get the path for retrieving the resource with the given resource ID.
     *
     * @param string $id The resource ID.
     *
     * @return string[] The retrieval path.
     */
    public function getRetrievePath($id)
    {
        return [
            $this->getBasePath(),
            $id,
        ];
    }

    abstract public function getType();

    public static function index(IndexOptions $options = null, ApiRequestor $requestor = null)
    {
        $empty = static::makeInternal(__METHOD__);
        $path = $empty->getIndexPath();

        $response = static::sendRequest($path, $options, $requestor);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Expected response body to be json');
        }

        $body = $response->getBody();

        if (!isset($body->data)) {
            $body->data = [];
        }

        if (isset($body->count)) {
            $count = $body->count;
        } else {
            $count = null;
        }

        $type = $empty->getType();
        $links = new LinkList(Obj::get($body, 'links', []));
        $items = [];
        foreach ($body->data as $responseItem) {
            if (isset($responseItem->data)) {
                $item = new static($responseItem->data);

                if (isset($responseItem->uri)) {
                    $item->setLinks([
                        'self' => $responseItem->uri,
                    ]);
                }
            } else {
                $item = new static($responseItem);
            }

            $items[] = $item;
        }

        $resourceList = new ApiResourceList($type, $items, $links, $count);

        return $resourceList;
    }

    public static function retrieve($id, RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $empty = static::makeInternal(__METHOD__);
        $path = $empty->getRetrievePath($id);

        $response = static::sendRequest($path, $options, $requestor);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Expected response body to be json');
        }

        $body = $response->getBody();

        $links = new LinkList(Obj::get($body, 'links', []));
        $data = $body->data;

        if ($empty->retrieveReturnsList()) {
            $zeroIndex = Obj::get($data, 0);
            $data = $zeroIndex->data;
        }

        $resource = new static($data);
        $resource->setLinks($links);

        return $resource;
    }

    public static function fetchResourceByUri($uri, Options $options = null, ApiRequestor $requestor = null)
    {
        $empty = static::makeInternal(__METHOD__);
        $basePath = $empty->getBasePath();

        $queryString = Uri::getQueryString($uri);
        if (Uri::isSamePath($basePath, $uri)) {
            // Index request
            if (!$options) {
                $options = IndexOptions::fromQueryString($queryString);
            } elseif ($options instanceof IndexOptions) {
                $options->loadQueryString($queryString);
            } else {
                throw new InvalidArgumentException('$options must be instanceof ' . IndexOptions::class);
            }

            return static::index($options, $requestor);
        } elseif (Uri::isDirectSubpath($basePath, $uri)) {
            // Retrieve request
            if (!$options) {
                $options = RetrieveOptions::fromQueryString($queryString);
            } elseif ($options instanceof RetrieveOptions) {
                $options->loadQueryString($queryString);
            } else {
                throw new InvalidArgumentException('$options must be instanceof ' . RetrieveOptions::class);
            }

            $parts = Uri::getPathParts($uri);
            $id = Arr::last($parts);

            return static::retrieve($id, $options, $requestor);
        }

        throw new InvalidArgumentException('Cannot fetch $uri in this resource');
    }

    public static function make($methodName = null)
    {
        return static::makeInternal(__METHOD__);
    }

    public static function supportsCount()
    {
        return true;
    }

    public static function supportsLimit()
    {
        return true;
    }

    /**
     * Override and return true if the Clever API returns a resource list instead of a singular resource.
     *
     * @return bool
     */
    protected function retrieveReturnsList()
    {
        return false;
    }

    private static function makeInternal($methodName)
    {
        $class = static::class;
        if ($class === self::class) {
            throw new BadMethodCallException($methodName . ' must be called from a base class');
        }

        return new $class();
    }

    private static function sendRequest($path, Options $options = null, ApiRequestor $requestor = null)
    {
        $requestor = DefaultApiRequestor::get($requestor);
        $apiOptions = new ApiOptions();
        if (!$options) {
            $options = new Options();
        }

        $apiOptions->queryParameters = $options->toQueryParameters();
        if ($options->getBearerToken()) {
            $apiOptions->bearerToken = $options->getBearerToken();
        }

        $bearerToken = $apiOptions->bearerToken;
        if ($options->getAutoWait()) {
            RateLimiter::get()->wait($bearerToken);
        }

        try {
            $response = $requestor->send($path, $apiOptions);
        } catch (ApiRequestException $e) {
            $rateLimit = $e->getResponse()->getRateLimit();
            if ($rateLimit) {
                RateLimiter::get()->update($bearerToken, $rateLimit);
            }

            throw $e;
        }

        $updatedRateLimit = $response->getRateLimit();
        if ($updatedRateLimit) {
            RateLimiter::get()->update($bearerToken, $updatedRateLimit);
        }

        return $response;

        // TODO: Auto retry
    }
}
