<?php

namespace Clever;

use Clever\Contracts\Linkable;
use Clever\Internal\AbstractList;
use Clever\Internal\LinkableTrait;
use InvalidArgumentException;

class ApiResourceList extends AbstractList implements Linkable
{
    use LinkableTrait;

    protected $count;
    protected $type;

    public function __construct($type, $items, $links, $count = null)
    {
        $this->setType($type);

        parent::__construct($this->processItems($items));

        $this->setLinks($links);
        $this->setCount($count);
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getLinkTypes()
    {
        return [
            'next' => $this->getType(),
            'prev' => $this->getType(),
        ];
    }

    public function getType()
    {
        return $this->type;
    }

    protected function processItems($items)
    {
        $output = [];
        $typeClass = Types::getTypeClass($this->getType());
        foreach ($items as $item) {
            if (!is_a($item, $typeClass)) {
                throw new InvalidArgumentException('All items must be of type '
                    . $this->getType() . '(' . $typeClass . ')');
            }
            $output[] = $item;
        }

        return $output;
    }

    protected function setCount($count)
    {
        if ($count === null) {
            $this->count = null;

            return;
        }

        if (!is_int($count)) {
            throw new InvalidArgumentException('Count must be int. Got ' . gettype($count));
        }

        if ($count < 0) {
            throw new InvalidArgumentException('Count must be positive. Got ' . $count);
        }

        $this->count = $count;
    }

    protected function setType($type)
    {
        if (!Types::has($type)) {
            throw new InvalidArgumentException('Unknown type ' . $type);
        }

        $this->type = $type;
    }
}
