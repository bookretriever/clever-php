<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Contracts\LastModified;
use Clever\Contracts\SisObject;
use Clever\Contracts\User;
use Clever\Internal\EventableTrait;
use Clever\Internal\LastModifiedTrait;
use Clever\Internal\SchoolResource;
use Clever\Internal\SisObjectTrait;
use Clever\Internal\UserTrait;
use Clever\Internal\Util;

class Student extends SchoolResource implements Eventable, LastModified, SisObject, User
{
    use EventableTrait;
    use LastModifiedTrait;
    use SisObjectTrait;
    use UserTrait;

    public function getDateOfBirth()
    {
        if (!$this->dob) {
            return null;
        }

        return Date::parseDate($this->dob);
    }

    public function getGender()
    {
        $gender = $this->gender;

        if (in_array($gender, ['M', 'F'], true)) {
            return $gender;
        }

        return null;
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getGradeNumeric()
    {
        $grade = $this->getGrade();

        return Util::gradeToNumeric($grade);
    }

    public function getStateId()
    {
        return $this->state_id;
    }

    public function getStudentNumber()
    {
        return $this->student_number;
    }

    public function getType()
    {
        return 'student';
    }
}
