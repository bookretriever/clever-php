<?php

namespace Clever;

use BadMethodCallException;
use Clever\Internal\Date;

class DistrictStatus extends ApiResource
{
    public function getError()
    {
        return $this->error;
    }

    public function getIndexPath()
    {
        throw new BadMethodCallException('Cannot index district statuses');
    }

    public function getIndexType()
    {
        return 'districts';
    }

    public function getInstantLogin()
    {
        return $this->get('instant_login');
    }

    public function getLastSync()
    {
        return Date::parseDateTime($this->get('last_sync'));
    }

    public function getRetrievePath($id)
    {
        return [
            $this->getBasePath(),
            $id,
            'status',
        ];
    }

    public function getType()
    {
        return 'district_status';
    }

    public function getSisType()
    {
        return $this->sis_type;
    }

    public function getState()
    {
        return $this->get('state');
    }

    public function hasDistrictFixableError()
    {
        return $this->getState() === 'pending';
    }

    public function isGood()
    {
        return $this->getState() === 'running';
    }

    public function retrieveReturnsList()
    {
        return true;
    }
}
