<?php

namespace Clever\Contracts;

use DateTime;

/**
 * Objects that have a last_modified property.
 */
interface LastModified
{
    /**
     * Gets the object's last modification time.
     *
     * @return DateTime The date & time the object was last modified.
     */
    public function getLastModified();
}
