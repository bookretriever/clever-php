<?php

namespace Clever\Contracts;

interface Linkable
{
    /**
     * Retrieve the LinkList object for this ApiResource.
     *
     * @return \Clever\LinkList
     */
    public function getLinks();

    /**
     * Retrieve the Link object with the given relation for this ApiResource.
     *
     * @return \Clever\Link
     *
     * @throws \Clever\Exceptions\UnknownLinkException
     */
    public function getLink($linkRelation);
}
