<?php

namespace Clever\Contracts;

/**
 * Objects that exist in the Student Information System (SIS) and have a SIS ID.
 */
interface SisObject
{
    /**
     * Gets the object's SIS ID.
     *
     * @return string The object's SIS ID.
     */
    public function getSisId();
}
