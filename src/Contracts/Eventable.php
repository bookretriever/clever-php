<?php

namespace Clever\Contracts;

/**
 * Resources that can have events.
 */
interface Eventable
{
    /**
     * Gets the resource's event type.
     *
     * @return string The resource's event type.
     */
    public function getEventType();
}
