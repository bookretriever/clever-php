<?php

namespace Clever\Contracts;

/**
 * Objects that belong to a district.
 */
interface DistrictObject
{
    /**
     * Gets the object's district ID.
     *
     * @return string The object's district ID.
     */
    public function getDistrictId();
}
