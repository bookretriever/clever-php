<?php

namespace Clever\Contracts;

use Clever\Name;

interface User extends DistrictObject
{
    /**
     * @return string|null The User's email, or null if not set.
     */
    public function getEmail();

    /**
     * @return string The User's ID, or null if not set.
     */
    public function getId();

    /**
     * @return Name|null The Name object for the User, or null if the user has no name
     */
    public function getName();

    /**
     * Gets the full name for the User.
     * Equivalent to calling getName()->getFull().
     *
     * @return string|null The full name for the User, or null if the user has no name.
     */
    public function getFullName();

    /**
     * @return string[] An array of the IDs of schools the User belongs to.
     */
    public function getSchoolIds();

    /**
     * @return string The User's title.
     */
    public function getTitle();

    /**
     * @return string The User's type.
     */
    public function getType();
}
