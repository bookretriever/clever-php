<?php

namespace Clever\Contracts;

/**
 * Objects that belong to a school.
 */
interface SchoolObject extends DistrictObject
{
    /**
     * Gets the object's school ID.
     *
     * @return string The object's school ID.
     */
    public function getSchoolId();
}
