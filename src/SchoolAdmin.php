<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Contracts\User;
use Clever\Internal\DistrictResource;
use Clever\Internal\EventableTrait;
use Clever\Internal\UserTrait;

class SchoolAdmin extends DistrictResource implements Eventable, User
{
    use EventableTrait;
    use UserTrait;

    public function getSchoolIds()
    {
        return $this->get('schools')->toArray();
    }

    public function getStaffId()
    {
        return $this->get('staff_id');
    }

    public function getType()
    {
        return 'school_admin';
    }
}
