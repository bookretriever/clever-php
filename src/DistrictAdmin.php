<?php

namespace Clever;

use Clever\Contracts\User;
use Clever\Internal\DistrictResource;
use Clever\Internal\UserTrait;

class DistrictAdmin extends DistrictResource implements User
{
    use UserTrait;

    public function getType()
    {
        return 'district_admin';
    }

    public static function supportsCount()
    {
        return false;
    }

    public static function supportsLimit()
    {
        return false;
    }
}
