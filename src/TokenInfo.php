<?php

namespace Clever;

use RuntimeException;

class TokenInfo extends CleverObject
{
    public function hasScopes($scopes)
    {
        $scopes = (array) $scopes;

        foreach ($scopes as $scope) {
            if (!in_array($scope, $this->get('scopes'), true)) {
                return false;
            }
        }

        return true;
    }

    public function verifyClientId($clientId)
    {
        if ($this->clientId === $clientId) {
            return true;
        }

        throw new RuntimeException('Client ID does not match');
    }
}
