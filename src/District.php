<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Internal\EventableTrait;

class District extends ApiResource implements Eventable
{
    use EventableTrait;

    public function getMdrNumber()
    {
        return $this->mdr_number;
    }

    public function getName()
    {
        return $this->get('name');
    }

    public function getType()
    {
        return 'district';
    }
}
