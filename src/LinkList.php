<?php

namespace Clever;

use Clever\Exceptions\UnknownLinkException;
use Clever\Internal\AbstractList;
use Clever\Internal\Arr;
use InvalidArgumentException;

class LinkList extends AbstractList
{
    public function __construct(array $links = [])
    {
        parent::__construct($this->parseLinks($links));
    }

    public function getLink($linkRelation)
    {
        if (!is_string($linkRelation) || !$linkRelation) {
            throw new InvalidArgumentException('$linkRelation must be a non-empty string');
        }

        if ($this->__isset($linkRelation)) {
            return $this->__get($linkRelation);
        }

        throw new UnknownLinkException('Unknown link \'' . $linkRelation . '\'');
    }

    public function setLinkType($linkRelation, $type)
    {
        $link = $this->getLink($linkRelation);
        $newLink = $link->setType($type);

        $newList = clone $this;
        $newList->data[$linkRelation] = $newLink;

        return $newList;
    }

    public function setLinkTypeTry($linkRelation, $type)
    {
        try {
            return $this->setLinkType($linkRelation, $type);
        } catch (UnknownLinkException $e) {
            return $this;
        }
    }

    private function parseLinks(array $links)
    {
        $createdLinks = [];
        foreach ($links as $relation => $link) {
            if (Arr::like($link)) {
                if (!Arr::has($link, 'rel')) {
                    if (is_string($relation)) {
                        Arr::set($link, 'rel', $relation);
                    } else {
                        throw new InvalidArgumentException('Cannot pass link without a relation');
                    }
                }

                $link = new Link($link);
            } elseif (is_string($link)) {
                $link = new Link([
                    'rel' => $relation,
                    'uri' => $link,
                ]);
            } elseif (!($link instanceof Link)) {
                throw new InvalidArgumentException('Each item in the array must be a link');
            }

            if (is_string($relation)) {
                if ($relation !== $link->getRelation()) {
                    throw new InvalidArgumentException('Link relation does not match');
                }
            } else {
                $relation = $link->getRelation();
            }

            $createdLinks[$relation] = $link;
        }

        return $createdLinks;
    }
}
