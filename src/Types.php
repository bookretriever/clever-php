<?php

namespace Clever;

use Clever\Request\ApiRequestor;
use Clever\Request\RetrieveOptions;
use InvalidArgumentException;

final class Types
{
    private static $registered = false;
    private static $registrations = [];

    public static function all()
    {
        static::register();

        return static::$registrations;
    }

    public static function getTypeClass($type)
    {
        if (static::has($type)) {
            return static::$registrations[$type];
        }

        return null;
    }

    public static function has($type)
    {
        static::register();

        return isset(static::$registrations[$type]);
    }

    public static function hasType($type)
    {
        return static::has($type);
    }

    public static function retrieve($type, $id, RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $class = static::getTypeClass($type);
        if (!$class) {
            throw new InvalidArgumentException('Unknown type ' . $type);
        }

        $empty = new $class();
        $resource = $empty->retrieve($id, $options, $requestor);

        return $resource;
    }

    private static function register()
    {
        if (static::$registered) {
            return;
        }

        $types = [
            'district' => District::class,
            'district_admin' => DistrictAdmin::class,
            'district_status' => DistrictStatus::class,
            'event' => Event::class,
            'section' => Section::class,
            'school' => School::class,
            'school_admin' => SchoolAdmin::class,
            'student' => Student::class,
            'teacher' => Teacher::class,
        ];

        static::$registrations = $types;
        static::$registered = true;
    }
}
