<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Contracts\LastModified;
use Clever\Contracts\SisObject;
use Clever\Internal\Date;
use Clever\Internal\EventableTrait;
use Clever\Internal\LastModifiedTrait;
use Clever\Internal\SchoolResource;
use Clever\Internal\SisObjectTrait;
use Clever\Internal\Util;

class Section extends SchoolResource implements Eventable, LastModified, SisObject
{
    use EventableTrait;
    use LastModifiedTrait;
    use SisObjectTrait;

    public function getCourseDescription()
    {
        return $this->course_description;
    }

    public function getCourseName()
    {
        return $this->course_name;
    }

    public function getCourseNumber()
    {
        return $this->course_number;
    }

    public function getGrade()
    {
        return $this->grade;
    }

    public function getGradeNumeric()
    {
        return Util::gradeToNumeric($this->getGrade());
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPeriod()
    {
        return $this->period;
    }

    public function getSectionNumber()
    {
        return $this->section_number;
    }

    public function getStudentIds()
    {
        return $this->get('students')->toArray();
    }

    public function getSubject()
    {
        return $this->get('subject');
    }

    public function getTeacherIds()
    {
        return $this->get('teachers')->toArray();
    }

    public function getTermEndDate()
    {
        $term = $this->term;

        if (!$term) {
            return null;
        }

        return Date::parseDate($term->end_date);
    }

    public function getTermName()
    {
        $term = $this->term;

        if (!$term) {
            return null;
        }

        return $term->name;
    }

    public function getTermStartDate()
    {
        $term = $this->term;

        if (!$term) {
            return null;
        }

        return Date::parseDate($term->start_date);
    }

    public function getType()
    {
        return 'section';
    }
}
