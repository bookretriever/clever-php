<?php

namespace Clever;

use Clever\Exceptions\UnknownTypeException;
use Clever\Internal\Arr;
use Clever\Request\ApiRequestor;
use Clever\Request\Options;
use InvalidArgumentException;

class Link extends CleverObject
{
    public function __construct($details)
    {
        $details = $this->parseDetails($details);
        parent::__construct($details);
    }

    public function __toString()
    {
        return $this->getUri();
    }

    public function getRelation()
    {
        return $this->rel;
    }

    public function getResource(Options $options = null, ApiRequestor $requestor = null)
    {
        $type = $this->getType();
        if (!$type) {
            throw new UnknownTypeException('Link has no type');
        } elseif (!Types::has($type)) {
            throw new UnknownTypeException('Link type \'' . $type . '\' is unknown.');
        }

        $class = Types::getTypeClass($type);

        return call_user_func($class . '::fetchResourceByUri', $this->getUri(), $options, $requestor);
    }

    public function getType()
    {
        return $this->type;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setType($type)
    {
        $this->checkType($type);

        return $this->updateData([
            'type' => $type,
        ]);
    }

    private function checkType($type)
    {
        if (!Types::has($type)) {
            throw new InvalidArgumentException('Invalid type' . (is_string($type) ? ': ' . $type : ''));
        }

        return true;
    }

    private function parseDetails($details)
    {
        $requiredKeys = [
            'rel',
            'uri',
        ];

        $optionalKeys = [
            'type',
        ];

        $parsedDetails = [];

        foreach ($requiredKeys as $key) {
            if (!Arr::has($details, $key)) {
                throw new InvalidArgumentException('$details.' . $key . ' must be set');
            }

            $value = Arr::get($details, $key);

            if (!is_string(Arr::get($details, $key))) {
                throw new InvalidArgumentException('$details.' . $key . ' must be a string');
            }

            $parsedDetails[$key] = $value;
        }

        foreach ($optionalKeys as $key) {
            if (!Arr::has($details, $key)) {
                continue;
            }

            $value = Arr::get($details, $key);

            if (!is_string(Arr::get($details, $key))) {
                throw new InvalidArgumentException('$details.' . $key . ' must be a string');
            }

            $parsedDetails[$key] = $value;
        }

        if (Arr::has($parsedDetails, 'type')) {
            $this->checkType(Arr::get($parsedDetails, 'type'));
        }

        return $parsedDetails;
    }

    private function updateData($updatedData)
    {
        $newLink = clone $this;
        foreach ($updatedData as $key => $value) {
            if ($value === null) {
                unset($newLink->data[$key]);
            }

            if (!is_scalar($value)) {
                throw new InvalidArgumentException('$updatedData must only consist of scalars');
            }

            $newLink->data[$key] = $value;
        }

        return $newLink;
    }
}
