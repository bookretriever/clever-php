<?php

namespace Clever;

use Clever\Exceptions\UnexpectedResponseException;
use Clever\Request\ApiOptions;
use Clever\Request\ApiRequestor;
use Clever\Request\DefaultApiRequestor;
use Clever\Request\Options;

class OAuth
{
    public static function me(Options $options, ApiRequestor $requestor = null)
    {
        $requestor = DefaultApiRequestor::get($requestor);
        $apiOptions = new ApiOptions();

        $path = [
            'me',
        ];

        if ($options->getBearerToken()) {
            $apiOptions->bearerToken = $options->getBearerToken();
        }

        $response = $requestor->send($path, $apiOptions);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Response from server is not JSON');
        }

        $body = $response->getBody();

        return new Me($body->data, $body->links);
    }

    public static function requestBearerToken($code, $redirectUri, ApiRequestor $requestor = null)
    {
        $requestor = DefaultApiRequestor::get($requestor);
        $apiOptions = new ApiOptions();
        $apiOptions->baseUrl = ApiOptions::BASE_URL_OAUTH;
        $apiOptions->method = 'post';

        $apiOptions->body = [
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $redirectUri,
        ];

        $path = [
            'oauth',
            'tokens',
        ];

        $response = $requestor->send($path, $apiOptions);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Response from server is not JSON');
        }

        $body = $response->getBody();
        if (isset($body->token_type)) {
            if ($body->token_type !== 'bearer') {
                $msg = 'Token received from server is not type \'bearer\'. ';
                if (is_string($body->token_type)) {
                    $msg .= 'Got \'' . $body->token_type . '\'.';
                } else {
                    $msg .= 'Got ' . gettype($body->token_type) . '.';
                }

                throw new UnexpectedResponseException($response, $msg);
            }
        } else {
            throw new UnexpectedResponseException($response, 'Response missing token_type');
        }

        if (isset($body->access_token)) {
            if (is_string($body->access_token)) {
                return $body->access_token;
            } else {
                throw new UnexpectedResponseException($response, 'access_token is not a string');
            }
        } else {
            throw new UnexpectedResponseException($response, 'Response missing access_token');
        }
    }

    public static function tokenInfo(Options $options, ApiRequestor $requestor = null)
    {
        $requestor = DefaultApiRequestor::get($requestor);
        $apiOptions = new ApiOptions();
        $apiOptions->baseUrl = ApiOptions::BASE_URL_OAUTH;

        $path = [
            'oauth',
            'tokeninfo',
        ];

        if ($options->getBearerToken()) {
            $apiOptions->bearerToken = $options->getBearerToken();
        }

        $response = $requestor->send($path, $apiOptions);
        if (!$response->isJson()) {
            throw new UnexpectedResponseException($response, 'Response from server is not JSON');
        }

        return new TokenInfo($response->getBody());
    }
}
