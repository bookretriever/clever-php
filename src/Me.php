<?php

namespace Clever;

use Clever\Contracts\Linkable;
use Clever\Internal\LinkableTrait;
use Clever\Request\ApiRequestor;
use Clever\Request\RetrieveOptions;

class Me extends CleverObject implements Linkable
{
    use LinkableTrait;

    public function __construct($data, $links)
    {
        parent::__construct($data);
        $this->setLinks($links);
    }

    public function getCanonical(RetrieveOptions $options = null, ApiRequestor $requestor = null)
    {
        $type = $this->getType();

        $link = $this->getLink('canonical');

        return $link->setType($type)->getResource($options);
    }

    public function getDistrictId()
    {
        return $this->get('district');
    }

    public function getId()
    {
        return $this->get('id');
    }

    public function getType()
    {
        return $this->get('type');
    }
}
