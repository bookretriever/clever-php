<?php

namespace Clever;

use Clever\Contracts\Eventable;
use Clever\Contracts\LastModified;
use Clever\Contracts\SisObject;
use Clever\Contracts\User;
use Clever\Internal\EventableTrait;
use Clever\Internal\LastModifiedTrait;
use Clever\Internal\SchoolResource;
use Clever\Internal\SisObjectTrait;
use Clever\Internal\UserTrait;

class Teacher extends SchoolResource implements Eventable, LastModified, SisObject, User
{
    use EventableTrait;
    use LastModifiedTrait;
    use SisObjectTrait;
    use UserTrait;

    public function getStateId()
    {
        return $this->state_id;
    }

    public function getTeacherNumber()
    {
        return $this->teacher_number;
    }

    public function getType()
    {
        return 'teacher';
    }
}
