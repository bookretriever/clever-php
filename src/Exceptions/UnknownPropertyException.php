<?php

namespace Clever\Exceptions;

use UnexpectedValueException;

class UnknownPropertyException extends UnexpectedValueException
{
}
