<?php

namespace Clever\Exceptions;

class UnexpectedResponseException extends ApiRequestException
{
}
