<?php

namespace Clever\Exceptions;

class ResourceNotFoundException extends ApiRequestException
{
}
