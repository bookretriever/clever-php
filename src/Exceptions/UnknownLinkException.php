<?php

namespace Clever\Exceptions;

class UnknownLinkException extends UnknownPropertyException
{
}
