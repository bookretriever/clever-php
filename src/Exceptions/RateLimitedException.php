<?php

namespace Clever\Exceptions;

class RateLimitedException extends ApiRequestException
{
    final public function getRateLimit()
    {
        return $this->getResponse()->getRateLimit();
    }
}
