<?php

namespace Clever\Exceptions;

use UnexpectedValueException;

class UnknownGradeException extends UnexpectedValueException
{
}
