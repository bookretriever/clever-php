<?php

namespace Clever\Exceptions;

class UnknownTypeException extends Exception
{
}
