<?php

namespace Clever\Exceptions;

use RuntimeException;

class Exception extends RuntimeException
{
}
