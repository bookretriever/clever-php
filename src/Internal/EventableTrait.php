<?php

namespace Clever\Internal;

trait EventableTrait
{
    /**
     * @see \Clever\Contracts\Eventable::getEventType()
     */
    public function getEventType()
    {
        $indexType = $this->getType();

        $eventType = preg_replace('/[^a-z]+/i', '', $indexType);
        $eventType .= 's';

        return $eventType;
    }
}
