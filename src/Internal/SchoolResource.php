<?php

namespace Clever\Internal;

use Clever\Contracts\SchoolObject;

/**
 * Convienence class to implement SchoolObject.
 */
abstract class SchoolResource extends DistrictResource implements SchoolObject
{
    /**
     * @see SchoolObject::getSchoolId()
     */
    public function getSchoolId()
    {
        return $this->get('school');
    }
}
