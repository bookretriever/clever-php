<?php

namespace Clever\Internal;

use Clever\Contracts\LastModified;

trait LastModifiedTrait
{
    /**
     * @see LastModified::getLastModified()
     */
    public function getLastModified()
    {
        $lastModified = $this->get('last_modified');

        return Date::parseDateTime($lastModified);
    }
}
