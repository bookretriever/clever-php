<?php

namespace Clever\Internal;

use Clever\CleverObject;

abstract class AbstractList extends CleverObject
{
    public function getFirst()
    {
        $items = $this->getItems();
        if (count($items)) {
            return $items[0];
        }

        return null;
    }

    public function getLast()
    {
        $items = $this->getItems();
        $count = count($items);
        if ($count) {
            return $items[$count - 1];
        }

        return null;
    }

    public function getItems()
    {
        return $this->getArrayToIterate();
    }
}
