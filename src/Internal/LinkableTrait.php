<?php

namespace Clever\Internal;

use Clever\LinkList;

trait LinkableTrait
{
    protected $links;

    /**
     * @see Clever\Linkable::getLinks()
     */
    public function getLink($linkRelation)
    {
        return $this->getLinks()->getLink($linkRelation);
    }

    /**
     * @see Clever\Linkable::getLinks()
     */
    public function getLinks()
    {
        return $this->links ?: new LinkList();
    }

    public function getLinkTypes()
    {
        return [];
    }

    protected function setLinks($links)
    {
        if ($links instanceof LinkList) {
            $this->links = $links;
        } elseif (is_array($links)) {
            $this->links = new LinkList($links);
        } else {
            throw new InvalidArgumentException('$links must be LinkedList or array of links');
        }

        foreach ($this->getLinkTypes() as $relation => $type) {
            $this->links = $this->links->setLinkTypeTry($relation, $type);
        }
    }
}
