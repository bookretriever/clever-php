<?php

namespace Clever\Internal;

use Clever\Exceptions\UnknownPropertyException;
use Clever\Name;

trait UserTrait
{
    /**
     * @see Clever\Contracts\User::getDistrictId()
     */
    public function getDistrictId()
    {
        return $this->get('district');
    }

    /**
     * @see Clever\Contracts\User::getEmail()
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @see Clever\Contracts\User::getFullName()
     */
    public function getFullName()
    {
        $name = $this->getName();
        if ($name) {
            return $name->getFull();
        }

        return null;
    }

    /**
     * @see Clever\Contracts\User::getName()
     */
    public function getName()
    {
        $name = $this->name;
        if ($name) {
            return new Name($name);
        }

        return null;
    }

    /**
     * @see Clever\Contracts\User::getSchoolIds()
     */
    public function getSchoolIds()
    {
        try {
            // Has multiple schools (school admin)
            return $this->get('schools')->toArray();
        } catch (UnknownPropertyException $e) {
            // Ignore
        }

        try {
            // Has single school (teacher or student)
            return [$this->get('school')];
        } catch (UnknownPropertyException $e) {
            // Ignore
        }

        // Has no schools (district admin)
        return [];
    }

    /**
     * @see Clever\Contracts\User::getTitle()
     */
    public function getTitle()
    {
        try {
            // Has multiple schools (school admin)
            return $this->get('title');
        } catch (UnknownPropertyException $e) {
            // Ignore
        }

        // Has no title (student)
        return null;
    }
}
