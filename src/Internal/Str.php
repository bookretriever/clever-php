<?php

namespace Clever\Internal;

use InvalidArgumentException;

final class Str
{
    const WHITESPACE_CHARS = " \t\n\r\0\x0B"; // From php.net/trim

    private function __construct()
    {
    }

    public static function contains($haystack, $needle)
    {
        if ($needle === '') {
            return true;
        }

        return strpos($haystack, $needle) !== false;
    }

    public static function endsWith($haystack, $needle)
    {
        $needleLen = strlen($needle);
        if ($needleLen === 0) {
            return true;
        }

        if (substr($haystack, -$needleLen) === $needle) {
            return true;
        }

        return false;
    }

    public static function startsWith($haystack, $needle)
    {
        $needleLen = strlen($needle);
        if ($needleLen === 0) {
            return true;
        }

        if (substr($haystack, 0, $needleLen) === $needle) {
            return true;
        }

        return false;
    }

    public static function trim($str, $toTrim)
    {
        if (is_string($toTrim)) {
            return trim($str, $toTrim);
        }

        if (!is_array($toTrim)) {
            throw new InvalidArgumentException('$toTrim muts be a string or array');
        }

        do {
            $modified = false;
            foreach ($toTrim as $trim) {
                $trimLen = strlen($trim);
                if (static::endsWith($str, $trim)) {
                    $str = substr($str, 0, strlen($str) - $trimLen);
                    $modified = true;
                }

                if (static::startsWith($str, $trim)) {
                    $str = substr($str, $trimLen);
                    $modified = true;
                }
            }
        } while ($modified);

        return $str;
    }
}
