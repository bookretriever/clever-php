<?php

namespace Clever\Internal;

use Clever\Contracts\SisObject;

trait SisObjectTrait
{
    /**
     * @see SisObject::getSisId()
     */
    public function getSisId()
    {
        return $this->get('sis_id');
    }
}
