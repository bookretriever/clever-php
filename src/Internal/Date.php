<?php

namespace Clever\Internal;

use Carbon\Carbon;

class Date
{
    private function __construct()
    {
    }

    public static function parseDate($dateStr)
    {
        $result = Carbon::parse($dateStr);
        $result->setTime(0, 0, 0);

        return $result;
    }

    public static function parseDateTime($dateTimeStr)
    {
        return Carbon::createFromFormat('Y-m-d\\TG:i:s.u\\Z', $dateTimeStr);
    }
}
