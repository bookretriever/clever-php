<?php

namespace Clever\Internal;

use Clever\ApiResource;
use Clever\Contracts\DistrictObject;

/**
 * Convienence class to implement DistrictObject.
 */
abstract class DistrictResource extends ApiResource implements DistrictObject
{
    /**
     * @see DistrictObject::getDistrictId()
     */
    public function getDistrictId()
    {
        return $this->get('district');
    }
}
