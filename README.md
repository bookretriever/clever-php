# Clever PHP Client Library
[![build status](https://gitlab.com/bookretriever/clever-php/badges/master/build.svg)](https://gitlab.com/bookretriever/clever-php/commits/master)

This is a rewrite of the official [Clever PHP Library](https://github.com/Clever/clever-php).
