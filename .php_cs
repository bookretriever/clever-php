<?php

$finder = Symfony\CS\Finder\DefaultFinder::create()
    ->in('src')
    ->in('tests')
;

return Symfony\CS\Config\Config::create()
    ->fixers([
        '-psr0',
        '-concat_without_spaces',
        '-empty_return',
        '-phpdoc_params',
    ])
    ->finder($finder)
;
