<?php

namespace CleverTests\Request;

use Clever\Request\IndexOptions;
use CleverTests\TestCase;

class IndexOptionsTest extends TestCase
{
    public function testLoadQueryString1()
    {
        $options = $this->fromQS('limit=5&ending_before=abc');
        $this->assertSame(5, $options->getLimit());
        $this->assertSame('abc', $options->getEndingBefore());
    }

    public function testLoadQueryString2()
    {
        $options = $this->fromQS('starting_after=abc');
        $this->assertSame('abc', $options->getStartingAfter());
    }

    public function testLoadQueryString3()
    {
        $options = $this->fromQS('count=true&limit=0');
        $this->assertSame(0, $options->getLimit());
        $this->assertTrue($options->getIncludeCount());
    }

    public function testLoadQueryString4()
    {
        $options = $this->fromQS('count=1');
        $this->assertNull($options->getIncludeCount());
    }

    private function fromQS($query)
    {
        return IndexOptions::fromQueryString($query);
    }
}
