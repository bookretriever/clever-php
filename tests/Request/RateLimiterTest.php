<?php

namespace CleverTests\Request;

use Clever\Request\RateLimiter;
use CleverTests\TestCase;

class RateLimiterTest extends TestCase
{
    const TEST_TOKEN = 'abc';

    public function testGet()
    {
        $first = RateLimiter::get();
        $second = RateLimiter::get();

        $this->assertSame($first, $second);
    }

    public function testGetRateLimitBlank()
    {
        $limiter = $this->make();
        $this->assertNull($limiter->getRateLimit(static::TEST_TOKEN));
    }

    public function testWait()
    {
        $this->markTestIncomplete();
    }

    public function testWaitNewToken()
    {
        $limiter = $this->make();
        $limiter->expects($this->never())
            ->method('clean');

        $limiter->wait('unknownToken');
    }

    private function make()
    {
        $mock = $this->getMockBuilder(RateLimiter::class)
            ->setMethods([
                'clean',
                'sleep',
                'usleep',
            ])
            ->getMock();

        $mock->method('sleep')
            ->willReturnCallback(function ($time) use ($mock) {
                $mock->clean();
            });

        $mock->method('usleep')
            ->willReturnCallback(function ($time) use ($mock) {
                $mock->clean();
            });

        return $mock;
    }
}
