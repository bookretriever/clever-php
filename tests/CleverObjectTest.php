<?php

namespace CleverTests;

use Clever\CleverObject;

class CleverObjectTest extends TestCase
{
    public function testDeepCreate()
    {
        $data = [
            'a' => [
                'b' => [
                    'c' => 1,
                    'd' => [
                        2,
                    ],
                ],
            ],
            'w' => json_decode(json_encode([
                'x' => [
                    'y',
                    'z' => [
                        1,
                    ],
                ],
            ])),
            'q' => [
                [
                    'r' => 's',
                ],
            ],
        ];

        $obj = new CleverObject($data);

        $this->assertInstanceOf(CleverObject::class, $obj->a);
        $this->assertInstanceOf(CleverObject::class, $obj->a->b);
        $this->assertInternalType('int', $obj->a->b->c);
        $this->assertSame(1, $obj->a->b->c);
        $this->assertSame(2, $obj->a->b->d->get(0));
        $this->assertInstanceOf(CleverObject::class, $obj->q->get(0));
    }
}
