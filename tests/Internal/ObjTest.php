<?php

namespace CleverTests\Internal;

use Clever\Internal\Obj;
use CleverTests\TestCase;
use stdClass;

class ObjTest extends TestCase
{
    /**
     * @dataProvider providerGet
     */
    public function testGet($key, $expected)
    {
        $obj = $this->makeObj();

        if (func_num_args() >= 3) {
            $default = func_get_arg(2);
            $value = Obj::get($obj, $key, $default);
        } else {
            $value = Obj::get($obj, $key);
        }

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerGet
     */
    public function testGetArr($key, $expected)
    {
        $arr = $this->makeArr();

        if (func_num_args() >= 3) {
            $default = func_get_arg(2);
            $value = Obj::get($arr, $key, $default);
        } else {
            $value = Obj::get($arr, $key);
        }

        $this->assertSame($expected, $value);
    }

    public function testSet()
    {
        $obj = $this->makeObj();

        Obj::set($obj, 'key2.a', 1);
        $this->assertSame(1, $obj->key2->a);

        Obj::set($obj, 'key4.b.c', 3);
        $this->assertSame(3, $obj->key4->b->c);
    }

    public function testSetArr()
    {
        $obj = $this->makeArr();

        Obj::set($obj, 'key2.a', 1);
        $this->assertSame(1, $obj['key2']['a']);

        Obj::set($obj, 'key4.b.c', 3);
        $this->assertSame(3, $obj['key4']['b']['c']);
    }

    public function providerGet()
    {
        return [
            ['key1', 'abc'],
            ['key3', null],
            ['key3', 'something', 'something'],
            ['missingKey', null],
            ['missingKey', 'something', 'something'],
            ['key5.x', 'y'],
            ['key5.missing', null],
            ['key5.missing', 'z', 'z'],
        ];
    }

    protected function makeArr()
    {
        $obj = [];

        $obj['key1'] = 'abc';
        $obj['key2'] = [];
        $obj['key3'] = null;
        $obj['key5'] = [];
        $obj['key5']['x'] = 'y';

        return $obj;
    }

    protected function makeObj()
    {
        $obj = new stdClass();

        $obj->key1 = 'abc';
        $obj->key2 = new stdClass();
        $obj->key3 = null;
        $obj->key5 = new stdClass();
        $obj->key5->x = 'y';

        return $obj;
    }
}
