<?php

namespace CleverTests\Internal;

use Carbon\Carbon;
use Clever\Exceptions\UnknownPropertyException;
use Clever\Internal\LastModifiedTrait;
use CleverTests\TestCase;
use DateTime;

/**
 * @coversDefaultClass Clever\Internal\LastModifiedTrait
 */
class LastModifiedTraitTest extends TestCase
{
    use DateTimeTestHelpers;
    use LastModifiedTrait;

    private $currentTimeString = null;

    /**
     * @covers ::getLastModified
     * @dataProvider providerTrait
     */
    public function testTrait($timeString, $expected)
    {
        $result = $this->pullLastModified($timeString);

        if (!($expected instanceof DateTime)) {
            throw new InvalidArgumentException('Expected $expected to be DateTime instance');
        }

        if (!($expected instanceof Carbon)) {
            $expected = Carbon::instance($expected);
        }

        $eq = $expected->eq($result);

        $this->assertTrue(
            $eq,
            $this->makeDateTimeExpectationString($expected, $result)
        );
    }

    public function testGet()
    {
        $this->currentTimeString = 'abc';
        $this->assertSame('abc', $this->get('last_modified'));
    }

    public function providerTrait()
    {
        return $this->getSampleDateTimeParameters();
    }

    private function get($property)
    {
        if ($property !== 'last_modified') {
            throw new UnknownPropertyException('Only expected last_modified to be retrieved');
        }

        return $this->currentTimeString;
    }

    private function pullLastModified($timeString)
    {
        $this->currentTimeString = $timeString;

        return $this->getLastModified();
    }
}
