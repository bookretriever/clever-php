<?php

namespace CleverTests\Internal;

use Carbon\Carbon;
use Clever\Internal\Date;
use CleverTests\TestCase;

/**
 * @coversDefaultClass Clever\Internal\Date
 */
class DateTest extends TestCase
{
    use DateTimeTestHelpers;

    /**
     * @covers ::parseDate
     * @dataProvider providerTestParseDate
     */
    public function testParseDate($dateString)
    {
        $expected = Carbon::createFromDate(2016, 1, 2, 'Etc/UTC');
        $expected->setTime(0, 0, 0);

        $actual = Date::parseDate($dateString);

        $this->assertTrue(
            $expected->eq($actual),
            'Expected ' . $expected->toDateTimeString()
                . '. Got ' . $actual->toDateTimeString()
        );
    }

    /**
     * @covers ::parseDateTime
     * @dataProvider providerTestParseDateTime
     */
    public function testParseDateTime($dateTimeString, $expected)
    {
        $actual = Date::parseDateTime($dateTimeString);

        $this->assertTrue(
            $expected->eq($actual),
            $this->makeDateTimeExpectationString($expected, $actual)
        );
    }

    public function providerTestParseDate()
    {
        return [
            ['01/02/2016'],
            ['2016-01-02'],
            ['2016-1-02'],
            ['2016-01-2'],
            ['2016-01-02T00:00:00.000Z'],
            ['2016-01-2 00:00:00.000000'],
            ['2016-01-2 00:00:00'],
            ['2016-01-2 02:05:34'],
        ];
    }

    public function providerTestParseDateTime()
    {
        return $this->getSampleDateTimeParameters();
    }
}
