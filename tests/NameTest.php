<?php

namespace CleverTests;

use Clever\Internal\Str;
use Clever\Name;

class NameTest extends TestCase
{
    /**
     * @dataProvider providerTestFormal
     */
    public function testFormal($format, $expected)
    {
        $value = $this->make($format)->getFormal();

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerTestFormalShort
     */
    public function testFormalShort($format, $expected)
    {
        $value = $this->make($format)->getFormalShort();

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerTestFull
     */
    public function testFull($format, $expected)
    {
        $value = $this->make($format)->getFull();

        $this->assertSame($expected, $value);
    }

    /**
     * @dataProvider providerTestFullShort
     */
    public function testFullShort($format, $expected)
    {
        $value = $this->make($format)->getFullShort();

        $this->assertSame($expected, $value);
    }

    public function testMake()
    {
        // Who tests the testers? The testers do!
        $name = $this->make('F');
        $this->assertSame('First', $name->first);
        $this->assertSame(null, $name->middle);
        $this->assertSame(null, $name->last);

        $name = $this->make('M');
        $this->assertSame(null, $name->first);
        $this->assertSame('Middle', $name->middle);
        $this->assertSame(null, $name->last);

        $name = $this->make('L');
        $this->assertSame(null, $name->first);
        $this->assertSame(null, $name->middle);
        $this->assertSame('Last', $name->last);
    }

    public function providerTestFormal()
    {
        return [
            ['FML', 'Last, First Middle'],
            ['FL', 'Last, First'],
            ['ML', 'Last, Middle'],
            ['FM', 'First Middle'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    public function providerTestFormalShort()
    {
        return [
            ['FML', 'Last, First'],
            ['FL', 'Last, First'],
            ['ML', 'Last'],
            ['FM', 'First'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    public function providerTestFull()
    {
        return [
            ['FML', 'First Middle Last'],
            ['FL', 'First Last'],
            ['ML', 'Middle Last'],
            ['FM', 'First Middle'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    public function providerTestFullShort()
    {
        return [
            ['FML', 'First Last'],
            ['FL', 'First Last'],
            ['ML', 'Last'],
            ['FM', 'First'],
            ['F', 'First'],
            ['L', 'Last'],
        ];
    }

    private function make($with = null)
    {
        if ($with === null) {
            $with = 'FML';
        }
        $values = [];
        if (Str::contains($with, 'F')) {
            $values['first'] = 'First';
        }

        if (Str::contains($with, 'M')) {
            $values['middle'] = 'Middle';
        }

        if (Str::contains($with, 'L')) {
            $values['last'] = 'Last';
        }

        return new Name($values);
    }
}
