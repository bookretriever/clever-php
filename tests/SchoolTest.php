<?php

namespace CleverTests;

use Clever\School;

class SchoolTest extends TestCase
{
    /**
     * @dataProvider providerIsMatchingGrade
     */
    public function testIsMatchingGrade($low, $high, $matchWith, $expected)
    {
        $school = new School([
            'low_grade' => $low,
            'high_grade' => $high,
        ]);

        $this->assertSame($expected, $school->isMatchingGrade($matchWith));
    }

    public function providerIsMatchingGrade()
    {
        return [
            ['Kindergarten', '3', '2', true],
            ['Kindergarten', '3', '3', true],
            ['Kindergarten', '3', 'PreKindergarten', false],
            ['Kindergarten', null, 'PreKindergarten', false],
            [null, '4', 'PreKindergarten', true],
            [null, null, null, true],
            [null, null, '5', true],
        ];
    }
}
