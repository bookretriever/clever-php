<?php

namespace CleverTests;

use Clever\Teacher;

class TeacherTest extends UserTest
{
    protected function assertSchoolIdCount($ids)
    {
        $this->assertCount(1, $ids);
    }

    protected function getUserClass()
    {
        return Teacher::class;
    }
}
