<?php

namespace CleverTests;

use Clever\SchoolAdmin;

class SchoolAdminTest extends UserTest
{
    protected function assertSchoolIdCount($ids)
    {
        $this->assertTrue(count($ids) >= 1);
    }

    protected function getUserClass()
    {
        return SchoolAdmin::class;
    }
}
