<?php

namespace CleverTests\Lib;

use Clever\CleverObject;
use Clever\Linkable;
use Clever\Request\ApiOptions;
use Psy\Configuration;
use Psy\Shell as PsyShell;

class Shell
{
    private $shell;

    public function run()
    {
        $this->setup();

        $this->shell->run();
    }

    public function setup()
    {
        ApiOptions::setDefault('bearerToken', 'DEMO_TOKEN');

        $this->shell = $this->makePsyShell();
    }

    public static function make()
    {
        return new static();
    }

    private function makePsyShell()
    {
        $config = new Configuration();
        $config->getPresenter()->addCasters($this->getCasters());

        $shell = new PsyShell($config);

        return $shell;
    }

    private function getCasters()
    {
        return [
            CleverObject::class => VarDumperCaster::class . '::castCleverObject',
            Linkable::class => VarDumperCaster::class . '::castLinkable',
        ];
    }
}
