<?php

namespace CleverTests\Lib;

use Clever\CleverObject;
use Clever\Linkable;
use Symfony\Component\VarDumper\Caster\Caster;

class VarDumperCaster
{
    public static function castCleverObject(CleverObject $obj, $array)
    {
        // TODO: Read actual data value instead of calling toArray
        foreach ($obj->toArray() as $key => $value) {
            $array[Caster::PREFIX_VIRTUAL . $key] = $value;
        }

        return $array;
    }

    public static function castLinkable(Linkable $obj, $array)
    {
        $array['links'] = $obj->getLinks();

        return $array;
    }
}
