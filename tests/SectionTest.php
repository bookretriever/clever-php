<?php

namespace CleverTests;

use Carbon\Carbon;
use Clever\Section;

class SectionTest extends TestCase
{
    public function testGetTermEndDate()
    {
        $section = $this->make();
        $expected = Carbon::create(2017, 3, 1, 0, 0, 0);

        $this->assertTrue($expected->eq($section->getTermEndDate()));
    }

    public function testGetTermEndDateEmpty()
    {
        $section = $this->makeEmpty();

        $this->assertNull($section->getTermEndDate());
    }

    public function testGetTermName()
    {
        $section = $this->make();

        $this->assertSame('Foo Bar', $section->getTermName());
    }

    public function testGetTermNameEmpty()
    {
        $section = $this->makeEmpty();

        $this->assertNull($section->getTermName());
    }

    public function testGetTermStartDate()
    {
        $section = $this->make();
        $expected = Carbon::create(2016, 1, 2, 0, 0, 0);

        $this->assertTrue($expected->eq($section->getTermStartDate()));
    }

    public function testGetTermStartDateEmpty()
    {
        $section = $this->makeEmpty();

        $this->assertNull($section->getTermStartDate());
    }

    private function make()
    {
        $details = [
            'term' => [
                'name' => 'Foo Bar',
                'start_date' => '2016-01-02',
                'end_date' => '2017-03-01',
            ],
        ];

        $section = new Section($details);

        return $section;
    }

    private function makeEmpty()
    {
        $details = [];

        $section = new Section($details);

        return $section;
    }

    public function providerIsMatchingGrade()
    {
        return [
            ['Kindergarten', '3', '2', true],
            ['Kindergarten', '3', '3', true],
            ['Kindergarten', '3', 'PreKindergarten', false],
            ['Kindergarten', null, 'PreKindergarten', false],
            [null, '4', 'PreKindergarten', true],
            [null, null, null, true],
            [null, null, '5', true],
        ];
    }
}
