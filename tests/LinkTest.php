<?php

namespace CleverTests;

use Clever\Link;
use InvalidArgumentException;

class LinkTest extends TestCase
{
    /**
     * @var Link
     */
    private $link;

    public function setUp()
    {
        $this->link = $this->make();
    }

    public function testCreate()
    {
        $this->make();
    }

    /**
     * @dataProvider providerLinkRequiredKeys
     */
    public function testCreateNotString($key)
    {
        $this->setExpectedException(InvalidArgumentException::class);

        $data = $this->getDefaultData();

        $data[$key] = 1;

        $this->make($data);
    }

    /**
     * @dataProvider providerLinkRequiredKeys
     */
    public function testCreateMisssing($key)
    {
        $this->setExpectedException(InvalidArgumentException::class);

        $data = $this->getDefaultData();

        unset($data[$key]);

        $this->make($data);
    }

    public function testGetRelation()
    {
        $this->assertSame($this->link->getRelation(), 'foo');
    }

    public function testGetUri()
    {
        $this->assertSame($this->link->getUri(), 'bar');
    }

    public function testSetType()
    {
        // Make sure we start off with a null type
        $this->assertNull($this->link->getType());

        $link = $this->link->setType('teacher');

        // Make sure we have a new object
        $this->assertNotEquals($link, $this->link);

        // Make sure the update applied correctly
        $this->assertSame('teacher', $link->getType());

        // We don't change the original link
        $this->assertNull($this->link->getType());
    }

    public function testToString()
    {
        $this->assertSame((string) $this->link, 'bar');
    }

    public function providerLinkRequiredKeys()
    {
        $keys = [
            'rel',
            'uri',
        ];

        $argLists = [];
        foreach ($keys as $key) {
            $argLists[] = [$key];
        }

        return $argLists;
    }

    private function getDefaultData(array $override = [])
    {
        $defaultData = [
            'rel' => 'foo',
            'uri' => 'bar',
        ];

        $defaultData = array_merge($defaultData, $override);

        return $defaultData;
    }

    private function make($data = 'default')
    {
        if ($data === 'default') {
            $data = $this->getDefaultData();
        }

        return new Link($data);
    }
}
