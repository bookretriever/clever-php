<?php

namespace CleverTests;

use Clever\ApiResourceList;

abstract class UserTest extends TestCase
{
    public function testList()
    {
        $teachers = $this->getUserList();

        $this->assertInstanceOf(ApiResourceList::class, $teachers);
        $this->assertNotEmpty($teachers);
    }

    public function testGetSchoolIds()
    {
        $teacher = $this->getUser();
        $ids = $teacher->getSchoolIds();

        $this->assertInternalType('array', $ids);
        $this->assertSchoolIdCount($ids);

        foreach ($ids as $id) {
            $this->assertInternalType('string', $id);
            $this->assertRegExp('/^[a-z0-9]{24}$/', $id);
        }
    }

    abstract protected function assertSchoolIdCount($ids);

    protected function getUser($id = null)
    {
        return $this->getResource($this->getUserClass(), $id);
    }

    protected function getUserList()
    {
        return $this->getResourceList($this->getUserClass());
    }

    abstract protected function getUserClass();
}
