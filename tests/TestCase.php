<?php

namespace CleverTests;

use Clever\Request\ApiOptions;
use PHPUnit_Framework_TestCase;

abstract class TestCase extends PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        parent::setUp();

        ApiOptions::setDefault('bearerToken', 'DEMO_TOKEN');
    }

    protected function getResource($resourceClass, $id = null)
    {
        if ($id === null) {
            $resources = $this->getResourceList($resourceClass);
            $resource = $resources->getFirst();
            if (!$resource) {
                throw new UnexpectedValueException('No resource of class \'' . $resourceClass . '\' was found');
            }

            $id = $resource->getId();
        }

        $resource = call_user_func($resourceClass . '::retrieve', $id);

        return $resource;
    }

    protected function getResourceList($resourceClass)
    {
        return call_user_func($resourceClass . '::index');
    }
}
