<?php

namespace CleverTests;

use Clever\Request\ApiOptions;
use Clever\Request\GuzzleApiRequestor;
use Clever\Request\Response;
use Psr\Http\Message\MessageInterface as PsrResponse;

class GuzzleApiRequestorTest extends TestCase
{
    public function testBasicRequest()
    {
        $requestor = new GuzzleApiRequestor();

        $options = new ApiOptions();
        $options->bearerToken = 'DEMO_TOKEN';

        $response = $requestor->send('/v1.1/students', $options);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertInstanceOf(PsrResponse::class, $response->getSourceResponse());
        $this->assertSame(200, $response->getStatusCode());
    }
}
