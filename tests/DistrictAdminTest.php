<?php

namespace CleverTests;

use Clever\DistrictAdmin;

class DistrictAdminTest extends UserTest
{
    protected function assertSchoolIdCount($ids)
    {
        $this->assertCount(0, $ids);
    }

    protected function getUserClass()
    {
        return DistrictAdmin::class;
    }
}
