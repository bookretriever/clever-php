<?php

namespace CleverTests;

use Clever\Link;
use Clever\LinkList;

class LinkListTest extends TestCase
{
    public function testCreateArray()
    {
        $list = new LinkList([
            'apple' => [
                'uri' => '/apple',
            ],
            [
                'rel' => 'orange',
                'uri' => 'something',
            ],
            'peach' => [
                'rel' => 'peach',
                'uri' => 'something else',
            ],
        ]);

        $this->assertCount(3, $list);
    }

    public function testCreateEmpty()
    {
        $list = new LinkList();
        $this->assertCount(0, $list);
    }

    public function testCreateMixed()
    {
        $list = new LinkList([
            new Link([
                'rel' => 'apple',
                'uri' => '/apple',
            ]),
            [
                'rel' => 'orange',
                'uri' => 'something',
            ],
        ]);

        $this->assertCount(2, $list);
    }

    public function testCreateStdClass()
    {
        $list = new LinkList([
            'pear' => $this->makeStdClass([
                'uri' => 'hello',
            ]),
        ]);

        $this->assertCount(1, $list);
    }

    public function testCreateString()
    {
        $list = new LinkList([
            'pear' => 'this is uri',
        ]);

        $this->assertCount(1, $list);
    }

    public function testCreateObjects()
    {
        $list = new LinkList([
            'apple' => new Link([
                'rel' => 'apple',
                'uri' => '/apple',
            ]),
            new Link([
                'rel' => 'orange',
                'uri' => 'something',
            ]),
        ]);

        $this->assertCount(2, $list);
    }

    public function testGetLink()
    {
        $list = $this->make();
        $this->assertInstanceOf(Link::class, $list->getLink('apple'));
    }

    public function testIterator()
    {
        $list = $this->make();

        $this->assertNotEmpty($list);

        $iterated = false;
        foreach ($list as $rel => $link) {
            $iterated = true;
            $this->assertInstanceOf(Link::class, $link);

            $this->assertSame($link->getRelation(), $rel);
        }

        $this->assertTrue($iterated, 'Expected iterator to run');
    }

    public function testSetLinkTypeTry()
    {
        $list = $this->make();
        $this->assertNull($list->getLink('apple')->getType());

        $list = $list->setLinkTypeTry('apple', 'teacher');
        $this->assertSame('teacher', $list->getLink('apple')->getType());
    }

    private function make()
    {
        return new LinkList($this->getDefaultData());
    }

    private function makeStdClass($data)
    {
        return json_decode(json_encode($data));
    }

    private function getDefaultData()
    {
        return [
            'apple' => [
                'uri' => '/apple',
            ],
            [
                'rel' => 'orange',
                'uri' => 'something',
            ],
            'peach' => [
                'rel' => 'peach',
                'uri' => 'something else',
            ],
        ];
    }
}
